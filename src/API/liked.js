const ROOT = "http://localhost:8080/liked";

export const getAllLiked = async (token) => {
  const response = await fetch(ROOT, {
    method: "GET",
    headers: {
      Authorization: `JWT ${token}`,
    },
  });
  const likedList = await response.json();
  return { status: response.status, likedList, message: response.message };
};

export const addToLiked = async (token, data) => {
  const response = await fetch(ROOT, {
    method: "POST",
    headers: {
      Authorization: `JWT ${token}`,
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify({ show: data }),
  });
  const { message } = await response.json();
  return { status: response.status, message };
};

export const deleteFromLikedList = async (token, id) => {
  const response = await fetch(`${ROOT}/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `JWT ${token}`,
    },
  });
  const { message } = await response.json();
  return { status: response.status, message };
};

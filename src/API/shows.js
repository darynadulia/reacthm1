const ROOT = "http://api.tvmaze.com";
const SCHEDULE = `${ROOT}/schedule`;
const SEARCH = `${ROOT}/search/shows`;
const SHOWS = `${ROOT}/shows`;

export const getSchedule = async () => {
  const response = await fetch(`${SCHEDULE}?data=2021-01-01`);
  const result = await response.json();
  return result;
};

export const getShowsBySearch = async (query) => {
  const response = await fetch(`${SEARCH}?q=${query}`);
  const result = await response.json();
  return result;
};

export const getShows = async () => {
  const response = await fetch(SHOWS);
  const result = await response.json();
  return result;
};

export const getShowById = async (id) => {
  const response = await fetch(`${SHOWS}/${id}`);
  const result = await response.json();
  return result;
};

export const getEpisodesById = async (id) => {
  const response = await fetch(`${SHOWS}/${id}/episodes`);
  const result = await response.json();
  return result;
};

export const getSeasonsById = async (id) => {
  const response = await fetch(`${SHOWS}/${id}/seasons`);
  const result = await response.json();
  return result;
};

export const getCastById = async (id) => {
  const response = await fetch(`${SHOWS}/${id}/cast`);
  const result = await response.json();
  return result;
};

export const getCrewById = async (id) => {
  const response = await fetch(`${SHOWS}/${id}/crew`);
  const result = await response.json();
  return result;
};

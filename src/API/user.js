const ROOT = "http://localhost:8080/user";
const PASSWORD = `${ROOT}/password`;

export const getProfile = async (token) => {
  const response = await fetch(ROOT, {
    method: "GET",
    headers: {
      Authorization: `JWT ${token}`,
    },
  });
  const { user } = await response.json();
  return { status: response.status, user, message: response.message };
};

export const deleteProfile = async (token) => {
  const response = await fetch(ROOT, {
    method: "DELETE",
    headers: {
      Authorization: `JWT ${token}`,
    },
  });
  const { message } = await response.json();
  return { status: response.status, message };
};

export const changePassword = async (data, token) => {
  const response = await fetch(PASSWORD, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      Authorization: `JWT ${token}`,
    },
    body: JSON.stringify(data),
  });
  const result = await response.json();
  return { status: response.status, message: result.message };
};

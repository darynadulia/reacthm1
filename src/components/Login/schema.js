import { object, string } from "yup";

const schema = object().shape({
  login: string().required("This field is required"),
  password: string()
    .required("This field is required")
    .min(6, "This field must be at least 6 characters long")
    .max(16, "This field must be up to 16 characters long"),
});

export default schema;

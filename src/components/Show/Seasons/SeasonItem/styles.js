const styles = (theme) => ({
  container: {
    padding: theme.spacing(1),
  },
  media: {
    width: "100%",
    height: "auto",
  },
});

export default styles;

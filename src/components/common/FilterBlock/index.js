// Core
import React from "react";
import { useLocation, useHistory } from "react-router-dom";
// Material-UI
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  makeStyles,
} from "@material-ui/core";
// Instruments
import genres from "./genres";
import routes from "../../../navigation/routes";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const FilterBlock = () => {
  const classes = useStyles();
  const history = useHistory();
  const queryParams = new URLSearchParams(useLocation().search);

  const onGenreChange = (event) => {
    if (event.target.value) {
      queryParams.set("genre", event.target.value);
    } else {
      queryParams.remove("genre");
    }
    history.push(`${routes.shows}?${queryParams}`);
  };

  return (
    <FormControl>
      <InputLabel>Genre</InputLabel>
      <Select onChange={onGenreChange} className={classes.select}>
        <MenuItem value="">None</MenuItem>
        {genres.map((item) => (
          <MenuItem value={item} key={`genre-${item}`}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

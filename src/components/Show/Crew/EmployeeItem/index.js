// Core
import React from "react";
import { string, shape } from "prop-types";
// Material-UI
import { Paper, Typography, makeStyles } from "@material-ui/core";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const EmployeeItem = ({ employee }) => {
  const classes = useStyles();
  return (
    <Paper elevation={4} className={classes.container}>
      {employee?.person?.image?.medium && (
        <img
          src={employee.person.image.medium}
          alt={employee.person.name}
          className={classes.media}
        />
      )}
      <Typography variant="h6">{employee.person.name}</Typography>
      <Typography variant="subtitle1">{employee.type}</Typography>
    </Paper>
  );
};

EmployeeItem.propTypes = {
  employee: shape({
    type: string.isRequired,
    person: shape({
      name: string.isRequired,
    }).isRequired,
    image: shape({
      medium: string,
      original: string,
    }),
  }).isRequired,
};

// Core
import React from "react";
import { shape, string, number, arrayOf } from "prop-types";
import { useHistory } from "react-router-dom";
// Material-UI
import {
  Box,
  Card,
  CardHeader,
  CardContent,
  Chip,
  makeStyles,
} from "@material-ui/core";
// Instruments
import routes from "../../../navigation/routes";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const ShowCard = ({ show }) => {
  const classes = useStyles();
  const history = useHistory();

  const openShow = () => {
    history.push(`${routes.shows}/${show.id}`);
  };
  return (
    <Box className={classes.card} onClick={openShow}>
      <Card>
        {show?.image?.medium && (
          <img
            className={classes.media}
            src={show.image.medium}
            alt={show.name}
          />
        )}

        <CardHeader title={show.name} subheader={show.status} />
        {Array.isArray(show.genres) && (
          <CardContent>
            {show.genres.map((genre) => (
              <Chip
                label={genre}
                color="secondary"
                className={classes.genre}
                key={`${show.name}-${genre}`}
              />
            ))}
          </CardContent>
        )}
      </Card>
    </Box>
  );
};

ShowCard.propTypes = {
  show: shape({
    id: number.isRequired,
    url: string.isRequired,
    name: string.isRequired,
    type: string.isRequired,
    genres: arrayOf(string).isRequired,
    image: shape({
      origin: string,
      medium: string,
    }),
  }).isRequired,
};

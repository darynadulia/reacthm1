const express = require("express");
const { asyncWrapper } = require("./helpers");
const authValidation = require("./middlewares/authValidation");
const authController = require("../controllers/authController");

const authRouter = new express.Router();

authRouter.post(
  "/register",
  asyncWrapper(authValidation.registration),
  asyncWrapper(authController.registration)
);

authRouter.post("/login", asyncWrapper(authController.login));

module.exports = authRouter;

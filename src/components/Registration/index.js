// Core
import React, { useState } from "react";
import { Form, Field, Formik } from "formik";
import { Link, useHistory } from "react-router-dom";
// Material-UI
import {
  Container,
  Typography,
  Box,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// Components
import { CustomTextField as TextField } from "../common/CustomTextField";
// Imstruments
import schema from "./schema";
import routes from "../../navigation/routes";
import { register } from "../../API/auth";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Registration = () => {
  const history = useHistory();
  const classes = useStyles();
  const [errorMessage, setErrorMessage] = useState();
  const token = localStorage.getItem("token");
  if (token) {
    history.push(routes.schedule);
  }
  return (
    <Container maxWidth="sm">
      <Formik
        initialValues={{ login: "", password: "", passwordConfirmation: "" }}
        validationSchema={schema}
        onSubmit={(value) => {
          const { login, password } = value;
          register({ login, password }).then((data) => {
            if (data.status === 200) {
              history.push(routes.login);
              setErrorMessage(null);
            } else {
              setErrorMessage(data.message);
            }
          });
        }}
      >
        <Form className={classes.container}>
          {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
          <Typography variant="h3" className={classes.title}>
            Registration
          </Typography>
          <Field
            component={TextField}
            id="login"
            name="login"
            label="Login"
            type="text"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Field
            component={TextField}
            id="password"
            name="password"
            label="Password"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Field
            component={TextField}
            id="passwordConfirmation"
            name="passwordConfirmation"
            label="Password confirmation"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Link to={routes.login} className={classes.link}>
            I have the profile
          </Link>
          <Box className={classes.buttonContainer}>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              className={classes.button}
            >
              Register
            </Button>
          </Box>
        </Form>
      </Formik>
    </Container>
  );
};

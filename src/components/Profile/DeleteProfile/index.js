// Core
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
// Material-UI
import {
  Checkbox,
  Button,
  Box,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// Instruments
import { deleteProfile } from "../../../API/user";
import routes from "../../../navigation/routes";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const DeleteProfile = () => {
  const classes = useStyles();
  const history = useHistory();
  const token = localStorage.getItem("token");
  const [checked, setChecked] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const changeChecking = () => {
    setChecked(!checked);
  };
  const onDeleteProfile = () => {
    deleteProfile(token).then((data) => {
      if (data.status === 200) {
        setErrorMessage(null);
        localStorage.removeItem("token");
        history.push(routes.schedule);
      } else {
        setErrorMessage(data.message);
      }
    });
  };
  return (
    <Box className={classes.container}>
      <Typography variant="h4" align="center" className={classes.title}>
        Delete profile
      </Typography>
      {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="center"
        alignContent="center"
      >
        <Checkbox checked={checked} onChange={changeChecking} />
        <Typography variant="body1">
          By choosing this item, I confirm my desire to completely delete the
          user profile.
        </Typography>
      </Box>
      <Box display="flex" flexDirection="row" justifyContent="center">
        <Button
          disabled={!checked}
          onClick={onDeleteProfile}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Delete my profile
        </Button>
      </Box>
    </Box>
  );
};

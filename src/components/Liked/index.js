// Core
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
// Material-UI
import {
  Container,
  Typography,
  Grid,
  Box,
  makeStyles,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
// Components
import { LikedItem } from "./LikedItem";
// Instruments
import routes from "../../navigation/routes";
import { getAllLiked } from "../../API/liked";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Liked = () => {
  const classes = useStyles();
  const history = useHistory();
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [list, setList] = useState();
  const location = useLocation();

  const limit = 6;
  const pages = list ? Math.ceil(list.length / limit) : 0;
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    setToken(localStorage.getItem("token"));
    if (!token) {
      history.push(routes.login);
    } else {
      getAllLiked(token).then((data) => {
        setList(data.likedList);
      });
    }
  }, [location]);

  const changePage = (event, value) => {
    setCurrentPage(value);
    window.scrollTo(0, 0);
  };
  return (
    <Container maxWidth="lg">
      {!list ? (
        <Typography variant="h6" align="center">
          Your list is still empty
        </Typography>
      ) : (
        <Grid container spacing={3}>
          {list
            .slice((currentPage - 1) * limit, (currentPage - 1) * limit + limit)
            .map((item) => (
              <Grid item md={6} xs={12} key={item.originalId}>
                <LikedItem liked={item} />
              </Grid>
            ))}
        </Grid>
      )}
      {pages > 1 && (
        <Box className={classes.paginationBox}>
          <Pagination
            count={pages}
            page={currentPage}
            onChange={changePage}
            color="secondary"
            size="large"
          />
        </Box>
      )}
    </Container>
  );
};

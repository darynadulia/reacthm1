/* eslint-disable no-underscore-dangle */
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../config");
const usersDao = require("../models/dao/userDao");

module.exports.registration = async (req, res) => {
  const { login, password } = req.body;
  await usersDao.postUser(login, password);
  res.json({ message: "Profile created successfully" });
};

module.exports.login = async (req, res) => {
  const { login, password } = req.body;

  const user = await usersDao.getUserByLogin(login);

  if (!(await bcrypt.compare(password, user.password))) {
    res.status(400).json({ message: "Wrong password" });
    return;
  }
  const token = jwt.sign(
    {
      _id: user._id,
      login: user.login,
      created_date: user.created_date,
    },
    JWT_SECRET
  );
  res.json({ jwt_token: token });
};

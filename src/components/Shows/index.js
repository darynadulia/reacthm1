// Core
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
// Material-UI
import {
  Container,
  Typography,
  Grid,
  Box,
  makeStyles,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
// Instruments
import { getShowsBySearch, getShows } from "../../API/shows";
// Components
import { ShowCard } from "../common/ShowCard";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Shows = () => {
  const classes = useStyles();
  const [shows, setShows] = useState(null);
  const queryParams = new URLSearchParams(useLocation().search);

  const q = queryParams.get("q");

  const limit = 12;
  const pages = shows ? Math.ceil(shows.length / limit) : 0;
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    if (q) {
      getShowsBySearch(q).then((data) => {
        setShows(data.map(({ show }) => show));
      });
    } else {
      getShows().then((data) => setShows(data));
    }
  }, [q]);
  const changePage = (event, value) => {
    setCurrentPage(value);
    window.scrollTo(0, 0);
  };
  return (
    <Container maxWidth="lg" className={classes.container}>
      {!shows && (
        <Typography variant="h6" alight="center">
          Wait few seconds
        </Typography>
      )}
      {Array.isArray(shows) && (
        <>
          <Typography variant="h2" align="center">
            Shows
          </Typography>

          <Grid container spacing={1}>
            {shows
              .slice(
                (currentPage - 1) * limit,
                (currentPage - 1) * limit + limit
              )
              .map((item) => (
                <Grid item md={3} sm={6} key={item.id}>
                  <ShowCard show={item} />
                </Grid>
              ))}
          </Grid>
        </>
      )}
      {pages > 1 && (
        <Box className={classes.paginationBox}>
          <Pagination
            count={pages}
            page={currentPage}
            onChange={changePage}
            color="primary"
            size="large"
          />
        </Box>
      )}
    </Container>
  );
};

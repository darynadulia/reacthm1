// Core
import React from "react";
import { shape, string, number, arrayOf } from "prop-types";
import { useHistory } from "react-router-dom";
// Material-UI
import {
  Box,
  Card,
  Paper,
  Grid,
  CardHeader,
  CardContent,
  Typography,
  Chip,
  makeStyles,
} from "@material-ui/core";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import ScheduleIcon from "@material-ui/icons/Schedule";
// Instruments
import routes from "../../../navigation/routes";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const ScheduleCard = ({ data }) => {
  const classes = useStyles();
  const history = useHistory();
  const handleClick = () => {
    history.push(`${routes.shows}/${data.show.id}`);
  };
  return (
    <Card
      className={classes.card}
      as={Paper}
      elevation={4}
      onClick={handleClick}
    >
      <Grid container spacing={1}>
        <Grid item md={2} xs={12}>
          <img
            className={classes.media}
            src={data.show.image.medium}
            alt={data.name}
          />
        </Grid>
        <Grid item md={10} xs={12}>
          <CardContent className={classes.content}>
            <CardHeader title={data.name} subheader={data.show.name} />

            {data.show.genres.map((genre) => (
              <Chip
                label={genre}
                color="primary"
                variant="outlined"
                key={`${data.show.name}-${genre}`}
              />
            ))}
            <Chip
              label={`Season: ${data.season}`}
              color="secondary"
              variant="outlined"
            />
            <Chip
              label={`Seria: ${data.number}`}
              color="secondary"
              variant="outlined"
            />
            <Box className={classes.dateTime}>
              <EventAvailableIcon />
              <Typography variant="subtitle1">{data.airdate}</Typography>
            </Box>
            <Box className={classes.dateTime}>
              <ScheduleIcon />
              <Typography variant="subtitle1">{data.airtime}</Typography>
            </Box>
            {data.summary ? (
              <Typography variant="body1">{data.summary}</Typography>
            ) : (
              <Typography variant="body1">{data.show.summary}</Typography>
            )}
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );
};

ScheduleCard.propTypes = {
  data: shape({
    id: number.isRequired,
    name: string.isRequired,
    season: number.isRequired,
    number: number.isRequired,
    airdate: string.isRequired,
    airtime: string.isRequired,
    airstamp: string.isRequired,
    summary: string,
    show: shape({
      id: number.isRequired,
      url: string.isRequired,
      name: string.isRequired,
      type: string.isRequired,
      genres: arrayOf(string).isRequired,
      image: shape({
        origin: string,
        medium: string,
      }),
    }).isRequired,
  }).isRequired,
};

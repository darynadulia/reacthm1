const styles = (theme) => ({
  paperContainer: {
    padding: theme.spacing(2),
    cursor: "pointer",
  },
  media: {
    maxWidth: "100%",
    height: "auto",
  },
});

export default styles;

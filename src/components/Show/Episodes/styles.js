const styles = (theme) => ({
  paginationBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    margin: theme.spacing(2),
  },
});

export default styles;

const mongoose = require("mongoose");

const likedSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  originalId: {
    type: Number,
    unique: true,
  },
  name: {
    type: String,
  },
  summary: String,
  type: String,
  status: String,
  genres: [
    {
      type: String,
    },
  ],
  image: {
    medium: String,
    original: String,
  },
});

module.exports.Liked = mongoose.model("Liked", likedSchema);

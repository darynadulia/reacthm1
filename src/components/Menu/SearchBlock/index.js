// Core
import React from "react";
import { useLocation, useHistory } from "react-router-dom";
// Material-UI
import { TextField, InputAdornment } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
// Instruments
import routes from "../../../navigation/routes";

export const SearchBlock = () => {
  const history = useHistory();
  const queryParams = new URLSearchParams(useLocation().search);

  const onChange = (event) => {
    if (!event.target.value) {
      history.push(routes.shows);
      queryParams.delete("q");
    } else {
      queryParams.set("q", event.target.value);
      history.push(`${routes.shows}?${queryParams}`);
    }
  };

  return (
    <TextField
      placeholder="шукати..."
      variant="outlined"
      color="secondary"
      size="small"
      onChange={(e) => onChange(e)}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start" color="secondary">
            <SearchIcon color="secondary" />
          </InputAdornment>
        ),
      }}
    />
  );
};

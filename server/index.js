const cors = require("cors");
const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const UnauthorizedError = require("./customErrors");

const PORT = 8080;

const authRouter = require("./routers/authRouter");
const userRouter = require("./routers/userRouter");
const likedRouter = require("./routers/likedRouter");

const app = express();

app.use(express.json());
app.use(morgan("tiny"));

app.use(cors());

app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/liked", likedRouter);

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({ message: err.message });
  }
  res.status(400).json({ message: err.message });
  next();
});

(async () => {
  await mongoose.connect(
    "mongodb+srv://testuser:testuser@cluster0.llydq.mongodb.net/reactTV?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    }
  );

  app.listen(PORT);
})();

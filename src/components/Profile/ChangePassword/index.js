// Core
import React, { useState } from "react";
import { Form, Field, Formik } from "formik";
// Material-UI
import {
  Container,
  Typography,
  Box,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// Components
import { CustomTextField as TextField } from "../../common/CustomTextField";
// Imstruments
import schema from "./schema";
import { changePassword } from "../../../API/user";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const ChangePassword = () => {
  const [errorMessage, setErrorMessage] = useState();
  const classes = useStyles();
  const token = localStorage.getItem("token");
  return (
    <Container maxWidth="sm">
      <Formik
        initialValues={{
          oldPassword: "",
          newPassword: "",
          passwordConfirmation: "",
        }}
        validationSchema={schema}
        onSubmit={(value, { resetForm }) => {
          setErrorMessage(null);
          const { oldPassword, newPassword } = value;
          changePassword({ oldPassword, newPassword }, token).then((data) => {
            if (data.status === 200) {
              setErrorMessage(null);
              resetForm();
            } else {
              setErrorMessage(data.message);
            }
          });
        }}
      >
        <Form>
          <Typography variant="h4" align="center">
            Change password
          </Typography>
          {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
          <Field
            component={TextField}
            id="oldPassword"
            name="oldPassword"
            label="Old password"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Field
            component={TextField}
            id="newPassword"
            name="newPassword"
            label="Password"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Field
            component={TextField}
            id="passwordConfirmation"
            name="passwordConfirmation"
            label="Password confirmation"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Box className={classes.buttonContainer}>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              className={classes.button}
            >
              Change password
            </Button>
          </Box>
        </Form>
      </Formik>
    </Container>
  );
};

class UnauthorizedError extends Error {
  constructor(message = "Unauthorized user!") {
    super(message);
    this.statusCode = 401;
  }
}

module.exports = UnauthorizedError;

// Core
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
// Material-UI
import { Grid, makeStyles } from "@material-ui/core";
// Components
import { SeasonItem } from "./SeasonItem";
// Instruments
import { getSeasonsById } from "../../../API/shows";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Seasons = () => {
  const classes = useStyles();
  const { id } = useParams();
  const [seasons, setSeasons] = useState();

  useEffect(() => {
    getSeasonsById(id).then((data) => setSeasons(data));
  }, [id]);

  return (
    <>
      {Array.isArray(seasons) && (
        <Grid container spacing={4} className={classes.container}>
          {seasons.map((item) => (
            <Grid item md={3} xs={12} key={item.id}>
              <SeasonItem season={item} />
            </Grid>
          ))}
        </Grid>
      )}
    </>
  );
};

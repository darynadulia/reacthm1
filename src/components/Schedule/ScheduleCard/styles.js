const styles = (theme) => ({
  card: {
    cursor: "pointer",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
  },
  dateTime: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    margin: theme.spacing(1),
  },
  media: {
    maxWidth: "100%",
    heigth: "auto",
  },
});

export default styles;

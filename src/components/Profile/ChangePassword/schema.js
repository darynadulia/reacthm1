import { object, string, ref } from "yup";

const schema = object().shape({
  oldPassword: string()
    .required("This field is required")
    .min(6, "This field must be at least 6 characters long")
    .max(16, "This field must be up to 16 characters long"),

  newPassword: string()
    .required("This field is required")
    .min(6, "This field must be at least 6 characters long")
    .max(16, "This field must be up to 16 characters long"),
  passwordConfirmation: string()
    .oneOf([ref("newPassword")], "The password you entered is incorrect")
    .required("This field is required"),
});

export default schema;

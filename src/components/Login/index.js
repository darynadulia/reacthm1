// Core
import React, { useState } from "react";
import { Form, Field, Formik } from "formik";
import { Link, useHistory } from "react-router-dom";
// Material-UI
import {
  Container,
  Typography,
  Box,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// Components
import { CustomTextField as TextField } from "../common/CustomTextField";
// Imstruments
import schema from "./schema";
import routes from "../../navigation/routes";
import { login } from "../../API/auth";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Login = () => {
  const classes = useStyles();
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState();
  const token = localStorage.getItem("token");
  if (token) {
    history.push(routes.schedule);
  }
  return (
    <Container maxWidth="sm">
      <Formik
        initialValues={{ login: "", password: "" }}
        validationSchema={schema}
        onSubmit={(value) => {
          login(value).then((data) => {
            if (data.status === 200) {
              localStorage.setItem("token", data.token);
              history.push(routes.schedule);
              setErrorMessage(null);
            } else {
              setErrorMessage(data.message);
            }
          });
        }}
      >
        <Form className={classes.container}>
          <Typography variant="h3" className={classes.title}>
            Login
          </Typography>
          {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
          <Field
            component={TextField}
            id="login"
            name="login"
            label="Login"
            type="text"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Field
            component={TextField}
            id="password"
            name="password"
            label="Password"
            type="password"
            fullWidth
            required
            variant="outlined"
            className={classes.field}
          />
          <Link to={routes.registration} className={classes.link}>
            I don&apos;t have my own profile yet
          </Link>
          <Box className={classes.buttonContainer}>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              className={classes.button}
            >
              Log in
            </Button>
          </Box>
        </Form>
      </Formik>
    </Container>
  );
};

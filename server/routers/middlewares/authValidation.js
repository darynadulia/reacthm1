const joi = require("joi");

module.exports.registration = async (req, res, next) => {
  const authSchema = joi.object({
    login: joi.string().required(),
    password: joi
      .string()
      .pattern(new RegExp("^[a-zA-Z0-9]{6,16}$"))
      .required(),
  });
  await authSchema.validateAsync(req.body);
  next();
};

module.exports.email = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email().required(),
  });
  await schema.validateAsync(req.body);
  next();
};

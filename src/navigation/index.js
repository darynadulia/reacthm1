// Core
import React, { useEffect } from "react";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";
// Components
import { Schedule } from "../components/Schedule";
import { Shows } from "../components/Shows";
import { Show } from "../components/Show";
import { Profile } from "../components/Profile";
import { Liked } from "../components/Liked";
import { Login } from "../components/Login";
import { Registration } from "../components/Registration";
// Instruments
import routes from "./routes";

export const Navigation = () => {
  const { pathname } = useLocation();
  const query = new URLSearchParams(useLocation().search);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname, query]);

  return (
    <Switch>
      <Route exact path={routes.schedule} component={Schedule} />
      <Route exact path={routes.shows} component={Shows} />
      <Route path={routes.show} component={Show} />
      <Route path={routes.profile} component={Profile} />
      <Route path={routes.liked} component={Liked} />
      <Route path={routes.login} component={Login} />
      <Route path={routes.registration} component={Registration} />
      <Redirect to={routes.schedule} />
    </Switch>
  );
};

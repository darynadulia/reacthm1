const styles = (theme) => ({
  container: {
    padding: theme.spacing(1),
  },
  media: {
    maxWidth: "100%",
    heigth: "auto",
  },
});

export default styles;

const styles = (theme) => ({
  container: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(4),
  },
  paginationBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    margin: theme.spacing(2),
  },
});

export default styles;

const ROOT = "http://localhost:8080/auth";
const LOGIN = `${ROOT}/login`;
const REGISTER = `${ROOT}/register`;

export const login = async (user) => {
  const response = await fetch(LOGIN, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(user),
  });
  // eslint-disable-next-line camelcase
  const { jwt_token: token, message } = await response.json();
  return { status: response.status, token, message };
};

export const register = async (user) => {
  const response = await fetch(REGISTER, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(user),
  });
  const result = await response.json();
  return { status: response.status, message: result.message };
};

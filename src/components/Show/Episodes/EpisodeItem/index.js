// Core
import React from "react";
import { string, number, shape } from "prop-types";
// Material-UI
import { Grid, Typography, Box, makeStyles } from "@material-ui/core";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import ScheduleIcon from "@material-ui/icons/Schedule";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const EpisodeItem = ({ episode }) => {
  const classes = useStyles();
  return (
    <Grid container spacing={2} className={classes.episode}>
      {episode?.image?.medium && (
        <Grid item md={2} xs={12}>
          <img
            src={episode.image.medium}
            alt={episode.name}
            className={classes.media}
          />
        </Grid>
      )}
      <Grid item md={episode?.image?.medium ? 10 : 12} xs={12}>
        <Typography variant="h5">{episode.name}</Typography>
        <Box className={classes.dateTime}>
          <EventAvailableIcon />
          <Typography variant="subtitle1">{episode.airdate}</Typography>
        </Box>
        <Box className={classes.dateTime}>
          <ScheduleIcon />
          <Typography variant="subtitle1">{episode.airtime}</Typography>
        </Box>
        <Typography variant="body1">{episode.summary}</Typography>
      </Grid>
    </Grid>
  );
};

EpisodeItem.propTypes = {
  episode: shape({
    id: number.isRequired,
    url: string.isRequired,
    name: string.isRequired,
    season: number.isRequired,
    number: number.isRequired,
    type: string,
    airdate: string.isRequired,
    airtime: string.isRequired,
    runtime: number,
    image: shape({
      medium: string,
      original: string,
    }),
    summary: string.isRequired,
  }).isRequired,
};

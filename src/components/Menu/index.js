// Core
import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
// Material-UI
import {
  AppBar,
  Toolbar,
  Box,
  Typography,
  Hidden,
  IconButton,
  Drawer,
  List,
  ListItem,
  Divider,
  makeStyles,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import FavoriteIcon from "@material-ui/icons/Favorite";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
// Instruments
import routes from "../../navigation/routes";
// Components
import { SearchBlock } from "./SearchBlock";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Menu = () => {
  const classes = useStyles();
  const location = useLocation();
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const openMobileMenu = () => setShowMobileMenu(true);
  const closeMobileMenu = () => setShowMobileMenu(false);
  useEffect(() => {
    setToken(localStorage.getItem("token"));
  }, [location]);

  const logOut = () => {
    localStorage.removeItem("token");
    setToken(null);
  };
  return (
    <AppBar color="primary" position="relative">
      <Toolbar className={classes.toolbar}>
        <Hidden mdDown>
          <Box display="flex" flexDirection="row">
            <Link to={routes.shows} className={classes.link}>
              <Typography variant="h5">SHOWS</Typography>
            </Link>
            <Link to={routes.schedule} className={classes.link}>
              <Typography variant="h5">SCHEDULE</Typography>
            </Link>
          </Box>
        </Hidden>
        <Hidden mdUp>
          <IconButton onClick={openMobileMenu}>
            <MenuIcon />
          </IconButton>
          <Drawer anchor="left" open={showMobileMenu} onClose={closeMobileMenu}>
            <List>
              <ListItem>
                <Link to={routes.shows} className={classes.link}>
                  <Typography variant="h5">SHOWS</Typography>
                </Link>
              </ListItem>
              <ListItem>
                <Link to={routes.schedule} className={classes.link}>
                  <Typography variant="h5">SCHEDULE</Typography>
                </Link>
              </ListItem>
              <Divider />
              {token ? (
                <>
                  <ListItem>
                    <Link to={routes.liked} className={classes.link}>
                      LIKED
                    </Link>
                  </ListItem>
                  <ListItem>
                    <Link to={routes.profile} className={classes.link}>
                      PROFILE
                    </Link>
                  </ListItem>
                  <IconButton onClick={logOut}>
                    <ExitToAppIcon />
                  </IconButton>
                </>
              ) : (
                <>
                  <ListItem>
                    <Link to={routes.login} className={classes.link}>
                      LOG IN
                    </Link>
                  </ListItem>
                  <ListItem>
                    <Link to={routes.registration} className={classes.link}>
                      REGISTRATION
                    </Link>
                  </ListItem>
                </>
              )}
            </List>
          </Drawer>
        </Hidden>
        <SearchBlock />
        <Hidden mdDown>
          {token ? (
            <Box display="flex" flexDirection="row">
              <Link to={routes.liked} className={classes.link}>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
              </Link>
              <Link to={routes.profile} className={classes.link}>
                <IconButton>
                  <AccountCircleIcon />
                </IconButton>
              </Link>
              <IconButton onClick={logOut}>
                <ExitToAppIcon />
              </IconButton>
            </Box>
          ) : (
            <Box display="flex" flexDirection="row">
              <Link to={routes.login} className={classes.link}>
                <Typography variant="h6">LOG IN</Typography>
              </Link>
              <Link to={routes.registration} className={classes.link}>
                <Typography variant="h6">REGISTRATION</Typography>
              </Link>
            </Box>
          )}
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

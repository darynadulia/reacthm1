// Core
import React from "react";
import { string, bool, func, shape } from "prop-types";
// Material-UI
import { TextField as MuiTextField } from "@material-ui/core";

export const CustomTextField = ({
  field,
  form: { touched, errors },
  ...props
}) => (
  <MuiTextField
    {...field}
    {...props}
    error={Boolean(touched[field.name] && errors[field.name])}
    helperText={errors[field.name]}
  />
);

CustomTextField.propTypes = {
  field: shape({
    name: string,
    value: string,
    onChange: func,
    onBlur: func,
  }).isRequired,
  form: shape({
    touched: shape({
      login: bool,
      password: bool,
    }),
    errors: shape({
      login: string,
      password: string,
    }),
  }).isRequired,
};

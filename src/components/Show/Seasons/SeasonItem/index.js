// Core
import React from "react";
import { string, number, shape } from "prop-types";
// Material-UI
import { Typography, Paper, makeStyles } from "@material-ui/core";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const SeasonItem = ({ season }) => {
  const classes = useStyles();
  return (
    <Paper elevation={4} className={classes.container}>
      {season?.image?.medium && (
        <img
          src={season.image.medium}
          alt={`${season.name}-${season.number}`}
          className={classes.media}
        />
      )}

      <Typography variant="h6">
        Season {season.number}. {season.name}
      </Typography>
      <Typography variant="subtitle1">
        Premiera: {season.premiereDate}
      </Typography>
      <Typography variant="subtitle1">End: {season.endDate}</Typography>
      <Typography variant="body1">{season.summary}</Typography>
    </Paper>
  );
};

SeasonItem.propTypes = {
  season: shape({
    number: number.isRequired,
    name: string,
    premiereDate: string,
    endDate: string,
    image: shape({
      medium: string,
      original: string,
    }),
    summary: string,
  }).isRequired,
};

// Core
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
// Material-UI
import { Grid } from "@material-ui/core";
// Instruments
import { getCrewById } from "../../../API/shows";
// Components
import { EmployeeItem } from "./EmployeeItem";

export const Crew = () => {
  const { id } = useParams();
  const [crew, setCrew] = useState();

  useEffect(() => {
    getCrewById(id).then((data) => setCrew(data));
  }, [id]);
  return (
    <>
      {Array.isArray(crew) && (
        <Grid container spacing={4}>
          {crew.map((item) => (
            <Grid item md={2} sm={6} xs={12} key={item.id}>
              <EmployeeItem employee={item} />
            </Grid>
          ))}
        </Grid>
      )}
    </>
  );
};

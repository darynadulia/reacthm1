// Core
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
// Material-UI
import { Grid, makeStyles } from "@material-ui/core";
// Components
import { ActorItem } from "./ActorItem";
// Instruments
import { getCastById } from "../../../API/shows";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Cast = () => {
  const classes = useStyles();
  const { id } = useParams();
  const [cast, setCast] = useState();
  useEffect(() => {
    getCastById(id).then((data) => setCast(data));
  }, [id]);
  return (
    <>
      {Array.isArray(cast) && (
        <Grid container spaing={2} className={classes.container}>
          {cast.map((item) => (
            <Grid item md={6} xs={12} key={item.id}>
              <ActorItem actor={item} />
            </Grid>
          ))}
        </Grid>
      )}
    </>
  );
};

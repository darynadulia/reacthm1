// Core
import React from "react";
import { BrowserRouter } from "react-router-dom";
// Materail-UI
import { ThemeProvider } from "@material-ui/core";
// Instruments
import { Navigation } from "./navigation/index";
import { theme } from "./theme";
// Components
import { Menu } from "./components/Menu";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Menu />
        <Navigation />
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;

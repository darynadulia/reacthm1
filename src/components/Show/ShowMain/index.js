// Core
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
// Material-UI
import {
  Box,
  Typography,
  Grid,
  Chip,
  Paper,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Rating, Alert } from "@material-ui/lab";
import FavoriteIcon from "@material-ui/icons/Favorite";
// Instruments
import { getShowById } from "../../../API/shows";
import { addToLiked } from "../../../API/liked";
// Styles
import styles from "../styles";

const useStyles = makeStyles((theme) => styles(theme));

export const ShowMain = () => {
  const classes = useStyles();
  const { id } = useParams();
  const [show, setShow] = useState();
  const [errorMessage, setErrorMessage] = useState();

  useEffect(() => {
    getShowById(id).then((data) => setShow(data));
  }, [id]);

  const onAddToLiked = () => {
    const token = localStorage.getItem("token");
    if (!token) {
      setErrorMessage("Please authorize in the system");
    } else {
      const {
        id: originalId,
        name,
        summary,
        image,
        type,
        status,
        genres,
      } = show;
      addToLiked(token, {
        originalId,
        name,
        summary,
        image,
        type,
        status,
        genres,
      }).then((data) => {
        if (data.status === 200) {
          setErrorMessage(null);
        } else {
          setErrorMessage(data.message);
        }
      });
    }
  };
  return (
    <>
      {show && (
        <Grid container spacing={4}>
          <Grid item md={3} xs={12}>
            <img
              src={show.image.original}
              alt={show.name}
              className={classes.media}
            />
          </Grid>
          <Grid item md={5} xs={12}>
            <Typography variant="h3">{show.name}</Typography>
            <Typography variant="body1" align="justify">
              {show.summary}
            </Typography>
          </Grid>

          <Grid item md={4} xs={12}>
            <Paper elevation={3} className={classes.informationPaper}>
              <Typography variant="subtitle1">Type: {show.type}</Typography>
              <Typography variant="subtitle1">Status: {show.status}</Typography>
              <Box>
                <Typography variant="subtitle1">Genres: </Typography>
                {show.genres.map((genre) => (
                  <Chip
                    label={genre}
                    color="secondary"
                    key={genre}
                    className={classes.genreChip}
                  />
                ))}
              </Box>
              <Typography variant="subtitle1">
                Premiered: {show.premiered}
              </Typography>
              <Box className={classes.rating}>
                <Rating
                  value={show.rating.average}
                  max={10}
                  precision={0.1}
                  size="small"
                  title={show.rating.average}
                  readOnly
                />
                {show.rating.average} ({show.weight} votes)
              </Box>
              <Button
                type="button"
                variant="contained"
                color="primary"
                onClick={onAddToLiked}
                fullWidth
              >
                Add to <FavoriteIcon />
              </Button>
            </Paper>
            {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
          </Grid>
        </Grid>
      )}
    </>
  );
};

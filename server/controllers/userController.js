/* eslint-disable no-underscore-dangle */
const bcrypt = require("bcrypt");
const usersDao = require("../models/dao/userDao");

module.exports.showProfile = (req, res) => {
  res.json({
    user: {
      _id: req.user._id,
      login: req.user.login,
      created_date: req.user.created_date,
    },
  });
};

module.exports.deleteProfile = async (req, res) => {
  await usersDao.deleteUser(req.user._id);
  res.json({ message: "Profile deleted successfully" });
};

module.exports.password = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await usersDao.getUserByLogin(req.user.login);
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({ message: "Wrong password" });
  }
  await usersDao.changePassword(user._id, newPassword);
  res.json({ message: "Success" });
};

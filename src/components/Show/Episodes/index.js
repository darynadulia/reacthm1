// Core
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
// Material-UI
import { Box, makeStyles } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
// Instruments
import { getEpisodesById } from "../../../API/shows";
// Components
import { EpisodeItem } from "./EpisodeItem";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Episodes = () => {
  const classes = useStyles();
  const { id } = useParams();
  const [episodes, setEpisodes] = useState();

  const limit = 5;
  const pages = episodes ? Math.ceil(episodes.length / limit) : 0;
  const [currentPage, setCurrentPage] = useState(1);
  const changePage = (event, value) => {
    setCurrentPage(value);
  };
  useEffect(() => {
    getEpisodesById(id).then((data) => setEpisodes(data));
  }, [id]);
  return (
    <>
      {Array.isArray(episodes) && (
        <Box className={classes.container}>
          {episodes
            .slice((currentPage - 1) * limit, (currentPage - 1) * limit + limit)
            .map((item) => (
              <EpisodeItem key={item.id} episode={item} />
            ))}
          {pages > 1 && (
            <Box className={classes.paginationBox}>
              <Pagination
                count={pages}
                page={currentPage}
                onChange={changePage}
                color="secondary"
                size="large"
              />
            </Box>
          )}
        </Box>
      )}
    </>
  );
};

const styles = (theme) => ({
  card: {
    maxWidth: 450,
    borderRadius: "10px 10px",
    cursor: "pointer",
  },
  "card:hover": {
    border: `8px solid ${theme.palette.primary.main}`,
  },
  media: {
    width: "100%",
    height: "auto",
  },
});

export default styles;

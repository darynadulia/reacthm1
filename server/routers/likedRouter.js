const express = require("express");
const { asyncWrapper } = require("./helpers");
const { authMiddleware } = require("./middlewares/authMiddleware");
const likedController = require("../controllers/likedController");

const likedRouter = new express.Router();

likedRouter.use(asyncWrapper(authMiddleware));

likedRouter.get("/", asyncWrapper(likedController.getListOfLiked));

likedRouter.post("/", asyncWrapper(likedController.addLiked));

likedRouter.delete("/:id", asyncWrapper(likedController.deleteLiked));

module.exports = likedRouter;

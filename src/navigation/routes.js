const routes = {
  schedule: "/schedule",
  shows: "/shows",
  show: "/shows/:id",
  profile: "/profile",
  liked: "/liked",
  login: "/login",
  registration: "/registration",
};

export default routes;

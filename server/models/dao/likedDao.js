const { Liked } = require("../likedModel");

module.exports.postLiked = async (data) => {
  const liked = new Liked(data);
  await liked.save();
};

module.exports.getAllLiked = async (userId) => {
  const listOfLiked = await Liked.find({ userId });
  if (!listOfLiked) {
    throw new Error("No liked for this user");
  }
  return listOfLiked;
};

module.exports.deleteLiked = async (_id) => {
  await Liked.findByIdAndDelete(_id);
};

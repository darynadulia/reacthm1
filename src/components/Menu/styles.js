const styles = (theme) => ({
  toolbar: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  link: {
    marginRight: theme.spacing(2),
    textDecoration: "none",
    color: theme.palette.secondary.main,
    "&:hover": {
      color: theme.palette.inherit.main,
    },
  },
});

export default styles;

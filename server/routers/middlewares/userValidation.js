const joi = require("joi");

module.exports.changePassword = async (req, res, next) => {
  const schema = joi.object({
    oldPassword: joi.string().pattern(new RegExp("^[a-zA-Z0-9]{6,16}$")),
    newPassword: joi.string().pattern(new RegExp("^[a-zA-Z0-9]{6,16}$")),
  });
  await schema.validateAsync(req.body);
  next();
};

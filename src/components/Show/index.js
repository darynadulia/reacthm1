// Core
import React, { useState } from "react";
// Material-UI
import { Container, AppBar, Tabs, Tab, makeStyles } from "@material-ui/core";
// Components
import { ShowMain } from "./ShowMain";
import { Episodes } from "./Episodes";
import { Seasons } from "./Seasons";
import { Cast } from "./Cast";
import { Crew } from "./Crew";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Show = () => {
  const classes = useStyles();
  const [value, setValue] = useState("Episodes");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Container maxWidth="lg" className={classes.container}>
      <ShowMain />
      <AppBar position="static" color="secondary">
        <Tabs value={value} onChange={handleChange} centered>
          <Tab label="Episodes" value="Episodes" />
          <Tab label="Seasons" value="Seasons" />
          <Tab label="Cast" value="Cast" />
          <Tab label="Crew" value="Crew" />
        </Tabs>
      </AppBar>
      {value === "Episodes" && <Episodes />}
      {value === "Seasons" && <Seasons />}
      {value === "Cast" && <Cast />}
      {value === "Crew" && <Crew />}
    </Container>
  );
};

const styles = (theme) => ({
  container: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(5),
  },
  title: {
    margin: theme.spacing(2),
  },
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    borderRadius: theme.spacing(4),
    color: theme.palette.inherit,
  },
});

export default styles;

const styles = (theme) => ({
  container: {
    marginTop: theme.spacing(4),
  },
  media: {
    maxWidth: "100%",
    height: "auto",
  },
  rating: {
    margin: theme.spacing(2),
    display: "flex",
    flexDirection: "row",
  },
  genreChip: {
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  informationPaper: {
    padding: theme.spacing(2),
  },
});

export default styles;

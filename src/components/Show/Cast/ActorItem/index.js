// Core
import React from "react";
import { string, shape } from "prop-types";
// Material-UI
import { Grid, Typography, makeStyles } from "@material-ui/core";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const ActorItem = ({ actor }) => {
  const classes = useStyles();
  return (
    <Grid container spasing={2}>
      <Grid item md={4} xs={6}>
        {actor.person?.image?.medium && (
          <img
            src={actor.person.image.medium}
            alt={actor.person.name}
            className={classes.media}
          />
        )}
      </Grid>
      <Grid item md={4} xs={6}>
        {actor.character?.image?.medium && (
          <img
            src={actor.character.image.medium}
            alt={actor.character.name}
            className={classes.media}
          />
        )}
      </Grid>
      <Grid item md={4} xs={12}>
        <Typography variant="h6">
          {actor.person.name} AS {actor.character.name}
        </Typography>
      </Grid>
    </Grid>
  );
};

ActorItem.propTypes = {
  actor: shape({
    person: shape({
      name: string.isRequired,
      image: shape({
        medium: string,
        original: string,
      }),
    }).isRequired,
    character: shape({
      name: string.isRequired,
      image: shape({
        medium: string,
        original: string,
      }),
    }).isRequired,
  }).isRequired,
};

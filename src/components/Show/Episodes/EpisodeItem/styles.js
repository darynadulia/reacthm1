const styles = (theme) => ({
  episode: {
    marginTop: theme.spacing(3),
  },
  media: {
    maxWidth: "100%",
    height: "auto",
  },
  dateTime: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    margin: theme.spacing(1),
  },
});

export default styles;

/* eslint-disable no-underscore-dangle */
// Core
import React, { useState } from "react";
import { string, number, arrayOf, shape } from "prop-types";
import { useHistory } from "react-router-dom";
// Material-UI
import {
  Card,
  Grid,
  CardActionArea,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Typography,
  Chip,
  makeStyles,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
// Instuments
import { deleteFromLikedList, addToLiked } from "../../../API/liked";
import routes from "../../../navigation/routes";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const LikedItem = ({ liked }) => {
  const classes = useStyles();
  const history = useHistory();
  const [deleteButton, setDeleteButton] = useState(true);
  const onDelete = () => {
    const token = localStorage.getItem("token");
    deleteFromLikedList(token, liked._id).then((data) => {
      if (data.status === 200) {
        setDeleteButton(false);
      }
    });
  };
  const onAddToLiked = () => {
    const token = localStorage.getItem("token");
    addToLiked(token, liked).then((data) => {
      if (data.status === 200) {
        setDeleteButton(true);
      }
    });
  };
  const openShow = () => {
    history.push(`${routes.shows}/${liked.originalId}`);
  };

  return (
    <Card className={classes.paperContainer}>
      <CardActionArea onClick={openShow}>
        <Grid container spacint={2}>
          <Grid item md={5} sx={12}>
            {liked?.image?.medium && (
              <img
                src={liked.image.medium}
                alt={liked.name}
                className={classes.media}
              />
            )}
          </Grid>
          <Grid item md={7} xs={12}>
            <CardHeader title={liked.name} subheader={liked.status} />
            <CardContent>
              <Typography variant="subtitle1">{liked.type}</Typography>
              {liked.genres.map((genre) => (
                <Chip label={genre} color="secondary" key={genre} />
              ))}
              <Typography variant="body1">{liked.summary}</Typography>
            </CardContent>
          </Grid>
        </Grid>
      </CardActionArea>
      <CardActions>
        {deleteButton ? (
          <Button
            onClick={onDelete}
            type="button"
            variant="contained"
            color="secondary"
            fullWidth
          >
            Remove from <FavoriteIcon />
          </Button>
        ) : (
          <Button
            type="button"
            variant="contained"
            color="primary"
            onClick={onAddToLiked}
            fullWidth
          >
            Add to <FavoriteIcon />
          </Button>
        )}
      </CardActions>
    </Card>
  );
};

LikedItem.propTypes = {
  liked: shape({
    _id: string.isRequired,
    userId: string.isRequired,
    originalId: number.isRequired,
    name: string.isRequired,
    summary: string,
    image: shape({
      medium: string,
      original: string,
    }),
    type: string.isRequired,
    status: string.isRequired,
    genres: arrayOf(string).isRequired,
  }).isRequired,
};

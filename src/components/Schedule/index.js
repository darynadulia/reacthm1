// Core
import React, { useEffect, useState } from "react";
// Material-UI
import { Container, Typography, Box, makeStyles } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
// Instruments
import { getSchedule } from "../../API/shows";
// Components
import { ScheduleCard } from "./ScheduleCard";
// Styles
import styles from "./styles";

const useStyles = makeStyles((theme) => styles(theme));

export const Schedule = () => {
  const classes = useStyles();
  const [shows, setShows] = useState(null);
  const limit = 6;
  const pages = shows ? Math.ceil(shows.length / limit) : 0;
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    getSchedule().then((data) => setShows(data));
  }, []);

  const changePage = (event, value) => {
    setCurrentPage(value);
    window.scrollTo(0, 0);
  };
  return (
    <Container maxWidth="lg" className={classes.container}>
      {!shows && (
        <Typography variant="h6" alight="center">
          Wait few seconds
        </Typography>
      )}
      {Array.isArray(shows) && (
        <>
          <Typography variant="h2" align="center">
            Schedule
          </Typography>

          {shows
            .slice((currentPage - 1) * limit, (currentPage - 1) * limit + limit)
            .map((item) => (
              <ScheduleCard data={item} key={item.id} />
            ))}
        </>
      )}
      {pages > 1 && (
        <Box className={classes.paginationBox}>
          <Pagination
            count={pages}
            page={currentPage}
            onChange={changePage}
            color="primary"
            size="large"
          />
        </Box>
      )}
    </Container>
  );
};

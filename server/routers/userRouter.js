const express = require("express");
const { asyncWrapper } = require("./helpers");
const { authMiddleware } = require("./middlewares/authMiddleware");
const userValidation = require("./middlewares/userValidation");
const userController = require("../controllers/userController");

const userRouter = new express.Router();

userRouter.use(asyncWrapper(authMiddleware));

userRouter.get("/", asyncWrapper(userController.showProfile));

userRouter.delete("/", asyncWrapper(userController.deleteProfile));

userRouter.patch(
  "/password",
  asyncWrapper(userValidation.changePassword),
  asyncWrapper(userController.password)
);

module.exports = userRouter;

const styles = (theme) => ({
  container: {
    magrinTop: theme.spacing(3),
    marginBottom: theme.spacing(4),
  },
});

export default styles;

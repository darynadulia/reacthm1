// Core
import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
// Material-UI
import { Container, Typography } from "@material-ui/core";
// Components
import { ChangePassword } from "./ChangePassword";
import { DeleteProfile } from "./DeleteProfile";
// Instruments
import routes from "../../navigation/routes";
import { getProfile } from "../../API/user";

export const Profile = () => {
  const history = useHistory();
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [profile, setProfile] = useState();
  const location = useLocation();

  useEffect(() => {
    setToken(localStorage.getItem("token"));
    if (!token) {
      history.push(routes.login);
    } else {
      getProfile(token).then((data) => {
        setProfile(data.user);
      });
    }
  }, [location]);

  return (
    <Container maxWidth="md">
      <Typography variant="h3" align="center">
        Login: {profile && profile.login}
      </Typography>
      <ChangePassword />
      <DeleteProfile />
    </Container>
  );
};

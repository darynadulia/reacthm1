const bcrypt = require("bcrypt");
const { User } = require("../userModel");

module.exports.postUser = async (login, password) => {
  const user = new User({
    login,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

module.exports.getUserByLogin = async (login) => {
  const user = await User.findOne({ login });
  if (!user) {
    throw new Error("No user with such login has been found");
  }
  return user;
};

module.exports.changePassword = async (_id, newPassword) => {
  await User.findByIdAndUpdate(_id, {
    $set: { password: await bcrypt.hash(newPassword, 10) },
  });
};

module.exports.deleteUser = async (_id) => {
  await User.findByIdAndDelete(_id);
};

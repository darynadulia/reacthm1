// Material-UI
import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#9e9d24",
    },
    secondary: {
      main: "#736372",
    },
    inherit: {
      main: "#efbcd5",
    },
  },
});

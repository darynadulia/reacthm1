/* eslint-disable no-underscore-dangle */
const likedDao = require("../models/dao/likedDao");

module.exports.addLiked = async (req, res) => {
  const { show } = req.body;
  await likedDao.postLiked({ userId: req.user._id, ...show });
  res.json({ message: "Added successfully" });
};

module.exports.getListOfLiked = async (req, res) => {
  const listOfLiked = await likedDao.getAllLiked(req.user._id);
  res.json(listOfLiked);
};

module.exports.deleteLiked = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    res.status(400).json({ message: "Bad request" });
  }
  await likedDao.deleteLiked(id);
  res.json({ message: "Deleted successfully" });
};
